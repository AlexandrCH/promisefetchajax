﻿$(document).on('change', "[name='QuizResultID']", function () {
    var quizResultID = $(this).val();
    var $parent = $(this).closest("[name='quiz_results']");

    testPromise(quizResultID, $parent)
        .then((text) => {
            console.log(text);
        },
        (error) => {
            console.log(error);
        });
});

function GetReportsBySelectedModule(quizResultID, parent) {
    SetLoaderText('Loading...');
    var userId = $("#UserID").val();
    var courseId = $(parent).find("[name='CourseID']").val();
    var url = '/Admin/GetQuizResultById';
    $.ajax({
        url: url,
        datatype: "json",
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        data: {
            userID: userId,
            courseID: courseId,
            quizResultID: quizResultID
        },
        success: function (data) {
            LoaderHide();
            $(parent).find("[name='quizResultInfo']").empty();
            $(parent).find("[name='quizResultInfo']").html(data);
            var attemptNumber = $(parent).find("[name='QuizResultID'] option:selected").text();
            $(parent).find("[name='attemptNumber']").html(attemptNumber);
        },
        error: function (xhr, status, error) {
            LoaderHide();
            console.log(xhr);
        }
    });
}

function testFetch(quizResultID, parent) {
    var userId = $("#UserID").val();
    var courseId = $(parent).find("[name='CourseID']").val();

    const URL = `/Admin/GetQuizResultById?userID=${userId}&courseID=${courseId}&quizResultID=${quizResultID}`;

    fetch(URL,
        {
            method: 'Get'
            //headers: {
            //    'Accept': 'application/json, text/plain, */*',
            //    'Content-Type': 'application/json'
            //},
        })
        .then((resp) => resp.text())
        .then((data) => {
            console.log(data);
            alert("ok");
        })
        .catch(error => {
            console.log(error);
            alert(error);
        });   
}

function testPromise(quizResultID, parent) {
    var userId = $("#UserID").val();
    var courseId = $(parent).find("[name='CourseID']").val();

    const URL = `/Admin/GetQuizResultById?userID=${userId}&courseID=${courseId}&quizResultID=${quizResultID}`;
    return new Promise(function (succeed, fail) {

        $.ajax({
            url: URL,
            datatype: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            data: {
                userID: userId,
                courseID: courseId,
                quizResultID: quizResultID
            },
            success: function (data) {
                succeed(data);
            },
            error: function (xhr, status, error) {
                fail(error);
            }
        });
    });
}